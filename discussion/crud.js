let http = require("http");

// Mock database

let directory = [
	{
		"name" : "Rendon",
		"email" : "rendon@gmail.com"
	},
	{
		"name" : "Jobert",
		"email" : "jobert@gmail.com"
	},
	{
		"name" : "CongTV",
		"email" : "congtv@gmail.com"
	},
	{
		"name" : "Pingris",
		"email" : "pingris@gmail.com"
	}
];

http.createServer(function(request, response){

	if (request.url == "/users" && request.method == "GET"){

		response.writeHead(200, {'Content-Type' : 'application/json'});
	
		response.write(JSON.stringify(directory));
	
		response.end();
	
	}

	// Route for creating a new item upon receiving a POST request.
	// A request object contains several parts:
		// Headers - contains information about the request context / content like what is the data type.
		// Body - contains the actual information being sent with the request.
		// Note : The request body can NOT be empty.

	else if(request.url == "/users" && request.method == "POST"){

		// Declare and initialize a "requestBody" variable to an empty string
		// This will act as a placeholder for the resource/data to be created later on.

		let requestBody = '';

		// A stream is a sequence of data
		// Data is received from the client and is processed in the 'data' stream.
		// The information provided from the request object enters a sequence called "data" code below will be triggered
		// data step - this reads the "data" stream and processes it as the request body.

		// on() method binds and event to an object
		// It is a way to express you intent if there is something happening (data sent or error in your case), then execute the function added as a parameter. This style of programming is called Event-driven programming.
		// event-driven-programming is a programming paradigm in which the flow of the program is determined by event such as user action(mouse clicks, key presses) or message passing from other programs or threads.


		request.on('data', function(data){

			requestBody += data
		});

		request.on('end', function(){

			// Check if at this point the requestBody is of data type STRING
			// We need this to be of data type JSON to access its property.

			console.log(typeof requestBody);

			// Convert the string requestBody to JSON
			// JSON.parse() - takes a JSON string and transform it into javascript objects.
			requestBody = JSON.parse(requestBody);

			// Create a new object representing the new mock database record

			let newUser = {
				"name" : requestBody.name,
				"email" : requestBody.email
			}

			// Add the user into the mock database
			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'Content-Type' : 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();

		});
	}
}).listen(4000);

console.log("Server running at localhost:4000");

